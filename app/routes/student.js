import Ember from 'ember';

export default Ember.Route.extend({
    model: function(params){
        console.log("getting specific student");
        return this.store.find('student', params.student_id);
    }
    
});
