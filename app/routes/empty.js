//redirect to this page when you want to load a white screen or clear the screen
import Ember from 'ember';

export default Ember.Route.extend({
    model(){
        return '';
    }
});
