import Ember from 'ember';

export default Ember.Controller.extend({
    init: function(){
        this._super;
        this.checkGender;
    },
    isMale: false,
    
    theFirstModel: Ember.computed.alias('model.firstObject'),
    
    theLastModel: Ember.computed.alias('model.lastObject'),
    
    actions:{
        checkGender: function(){
            if(this.get('model.gender') == 1){
                this.set('isMale', true)
            }
            else{
                this.set('isMale', false)
            }
        }   
    }
    
});
