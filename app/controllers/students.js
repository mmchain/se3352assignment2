import Ember from 'ember';

export default Ember.Controller.extend({
    
    actions:{
        store: Ember.inject.service(),
        routing: Ember.inject.service('-routing'),
        
        search: function(num){
            
            
            var stu = this.store.query('student', { filter: { number: num } }).then(function(students) {
              // do something with `student`
              
              //let searchID = student.get('id')
              
              //console.log("getting id " + searchID);
                //this.get('routing').transitionTo('student', searchID);
                let st = students.get('firstObject');
                console.log("Currently logged in as " + st);
                this.transitionToRoute('student', st.id);
                return st
            });
            
            console.log("getting id " + stu);
            //this.get('routing').transitionTo('student', stu);
            
            
        }
        
    }
});
