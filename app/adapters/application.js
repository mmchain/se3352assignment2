import DS from 'ember-data';

export default DS.RESTAdapter.extend({
    host : 'https://se3352assignment2-chuck--taylor.c9users.io:8081'
});
