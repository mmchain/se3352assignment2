import DS from 'ember-data';

export default DS.Model.extend({
     studentNumber: DS.attr(),
  firstName: DS.attr(),
  lastName: DS.attr(),
  DOB: DS.attr(),
  residency: DS.attr(),
    gender: DS.attr(),
});
