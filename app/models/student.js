import DS from 'ember-data';

export default DS.Model.extend({
    number: DS.attr('number'),
    firstName: DS.attr(),
    lastName: DS.attr(),
    gender: DS.attr('number'),
    DOB: DS.attr(),
    residency: DS.attr('number'),
});
