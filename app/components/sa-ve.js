import Ember from 'ember';

export default Ember.Component.extend({

    actions: {
    
    save: function (id) {
      var myStore = this.get('store');
      console.log(this.get)
      var self = this;

      myStore.findRecord('student', id).then(function (student) {
        student.set('number', self.get('selectedStudent.number'));
        student.set('firstName', self.get('selectedStudent.firstName'));
        student.set('lastName', self.get('selectedStudent.lastName'));
        student.set('gender', self.get('selectedStudent.gender'));
        student.set('DOB', self.get('selectedStudent.DOB'));
        student.set('residency', self.get('selectedStudent.residency'));
        student.save();  // => PATCH to /students/:student_id
      })
    }
    }
});
