import Ember from 'ember';

export default Ember.Component.extend({
      searchID: Ember.computed.alias('controllers.students.searchID'),
actions: {
        
        oldSearch: function (num) {
            var myStore = this.get('store');
            var studentID = null;
            var self = this;
    
            myStore.queryRecord('student', { filter: { number: num } }).then(function(student) {
              // do something with `student`
              this.set('studentID', student.get('id'))
              this.get('routing').transitionTo('student', studentID);
            });
        return studentID
        }
    },
    
    nextSearch: function (num) {
      sendAction('search', num);
    }
});
