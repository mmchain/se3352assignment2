import Ember from 'ember';

export default Ember.Component.extend({
    store: Ember.inject.service(),
    routing: Ember.inject.service('-routing'),

studentID : null,
actions: {
    
    save: function (id) {
      var myStore = this.get('store');

      var self = this;

      myStore.findRecord('student', id).then(function (student) {
        student.set('number', self.get('selectedStudent.number'));
        student.set('firstName', self.get('selectedStudent.firstName'));
        student.set('lastName', self.get('selectedStudent.lastName'));
        student.set('gender', self.get('selectedStudent.gender'));
        student.set('DOB', self.get('selectedStudent.DOB'));
        student.set('residency', self.get('selectedStudent.residency'));
        student.save();  // => PATCH to /students/:student_id
      });
      
      if(this.get('selectedStudent.gender') == 1){
        this.set('isMale', true)
      }
      else{
        this.set('isMale', false)
      }
    },
    
    checkGender: function(){
      if(this.get('selectedStudent.gender') == 1){
        this.set('isMale', true)
      }
      else{
        this.set('isMale', false)
      }
    },
    
    
    
    
    returnGender: function(){
      if(this.get('selectedStudent.gender') == 1){
        return true
      }
      else{
        return false
      }
    },
    
    undo: function(){
      this.get('selectedStudent').rollbackAttributes();
    },
    
    getFirst: function() {
      
      $.get('https://se3352assignment2-chuck--taylor.c9users.io:8081/students', function(data){
        console.log(data.student[0]._id);
        location.href = '/student/' + (data.student[0]._id);
      })
      
      
    },
    
    getLast: function() {
      
       $.get(':8081/students', function(data){
        console.log(data.student[0]._id);
        location.href = '/student/' + (data.student[data.student.length -1]._id);
        
      })
    },
    
    nextRecord: function(id) {
      
      $.get('https://se3352assignment2-chuck--taylor.c9users.io:8081/students/getnext/' + id, function(data){
        location.href = '/student/' + (data.student[0]._id);
        
      })
    },
    
    prevRecord: function(id) {
      
      $.get('https://se3352assignment2-chuck--taylor.c9users.io:8081/students/getprev/' + id, function(data){
        location.href = '/student/' + (data.student[0]._id);
        
      })
    }
    
    
    }
});
