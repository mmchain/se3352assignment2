import Ember from 'ember';

export default Ember.Component.extend({
    
    showList: true,
    

  actions: {
    displayList: function() {
      this.set('showList', false);
    },

    hideList: function() {
      this.set('showList', true);
    }
  }
  
  
});
