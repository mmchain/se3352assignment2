import Ember from 'ember';

export default Ember.Component.extend({
  
    showHelp: true,
    

  actions: {
    displayHelp: function() {
      this.set('showHelp', false);
    },

    hideHelp: function() {
      this.set('showHelp', true);
    }
  }
  
});
